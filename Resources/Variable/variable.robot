*** Variable ***
#GLOBAL VARIABLE
${URL}              https://reqres.in
${CONTENT_TYPE}     application/json
${USER_AGENT}       Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.1000 Mobile Safari/537.36

#GET ALL USERS
${URL_GET_ALL_USERS}    /api/users?page=22