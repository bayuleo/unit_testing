*** Settings ***
Library     RequestsLibrary
Resource    ../Variable/variable.robot

*** Keywords ***
hit Api to showing all user from Api
	[Documentation]		Keywords to get proses Api
	Create Session		GetAllUser	${URL}	verify=True
	${header}=			Create Dictionary	content-type=${CONTENT_TYPE}	User-Agent=${USER_AGENT}
	${response}=   		Get Request	GetAllUser	uri=${URL_GET_ALL_USERS}	headers=${header}
	Should Be Equal As Integers		${response.status_code}		200
	Log		${response.json()}